var express = require('express');
var router = express.Router();

/* GET json page. */

router.get('/', function(req, res, next) {
  res.json({content: "Hello World! (again)"});
});

module.exports = router;