#!/bin/bash
#
#
# MODE DEBUG
set -x

#echo "Filename: $1";
filename="$1";
cd || { echo "Failed : Go to root" > log.txt; exit 1; }

cd stuffs/express-project || { echo "Failed : go to my project directory" >> log.txt; exit 1; }

#if [[ -f "$filename" ]]; then
#  cp -fr "$filename" /home/zenika || { echo "Failed : overwrite my file"; exit 1; }
# else
# cp "$filename" /home/zenika || { echo "Failed : copy my file"; exit 1; }
# fi

cp -fr "$filename" /home/zenika || { echo "Failed : overwrite my file" >> log.txt; exit 1; }

tar --extract -f "$filename" || { echo "Failed : extract the archive" >> log.txt; exit 1; }

npm start || { echo "Failed : npm start" >> log.txt; exit 1; }


# basename / dirname