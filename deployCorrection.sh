#!/usr/bin/env bash
#
# MODE DEBUG
# set -x

if [[ -z $1 ]]; then
    echo "Parameter required"
    exit 1
fi
# Filename to use
FILENAME=$1
# Create the project directory
if mkdir -p "$HOME/project"; then
    echo "directory created"
else
    echo "failed to create directory"
    exit 1
fi

if cp -f "$FILENAME" "$HOME/project"; then
    echo "copy succeeded"
else
    echo "failed to copy project"
    exit 1
fi

REAL_FILENAME="$(basename "$FILENAME")"
cd "$HOME/project" || {
    echo "Folder $HOME/project do not exists"
    exit 1
}

if tar xfz "$HOME/project/$REAL_FILENAME"; then
    :
else
    echo "Failed to extract $HOME/project/$REAL_FILENAME"
    exit 1
fi

echo "Start the server"
npm start
