#!/usr/bin/env bash

cd || { echo "Failed : Go to root"; exit 1; }
cd /tmp || { echo "Failed : Change directory to /tmp"; exit 1; }
mkdir myNewFolder || { echo "Failed : Create myNewFolder"; exit 1; }
cd myNewFolder || { echo "Failed : Change directory to myNewFolder"; exit 1; }
git clone https://gitlab.com/Arina-S-dev/express-project.git || { echo "Failed : git clone"; exit 1; }
cd express-project || { echo "Failed : Change directory to express-project"; exit 1; }
npm install --production || { echo "Failed : npm install production"; exit 1; }
touch version.txt || { echo "Failed : Create version.txt"; exit 1; }
echo "1.0.0" >> version.txt || { echo "Failed : Add content to version.txt"; exit 1; }
cd - || { echo "Failed : Change directory to root"; exit 1; }
tar czf express-project.tar.gz express-project || { echo "Failed : Create an archive tar.gz"; exit 1; }
chmod +rwx express-project.tar.gz || { echo "Failed : Change rwd archive"; exit 1; }
mv express-project.tar.gz /home/zenika || { echo "Failed : Move the archive to /home"; exit 1; }
cd || { echo "Failed : Go to root"; exit 1; }
cd /tmp || { echo "Failed : Change directory to /tmp"; exit 1; }
rm -r -f myNewFolder/ || { echo "Failed : remove myNewFolder"; exit 1; }

#
#
# echo coucou ; echo tata = les deux s'executent
# echo coucou && echo tata = le deuxieme s'execute uniquement si le premier est OK
# echo coucou && ls /jenexistepas || echo tata = tata s'execute si le deuxieme n'est pas OK
# echo $? = affiche si la dernière commande s'est bien executée (0 = OK, 1 = pas OK)
#
#



