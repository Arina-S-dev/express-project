#!/usr/bin/env bash
#

# A function: remove the created tmp directory
cleanup() {
    rm -rf "$TMP_DIR"
}

# Create the tmp directory
TMP_DIR="$(mktemp -d -t /tmp)"

cd "$TMP_DIR" || {
    echo "Folder $TMP_DIR do not exists"
    cleanup
    exit 1
}

if git clone https://gitlab.com/frederic.barrau/bidule.git; then
    echo "Project cloned succesfully"
else
    echo "Failed to clone the project"
    cleanup
    exit 1
fi

cd bidule || {
    echo "Folder $TMP_DIR do not exists"
    cleanup
    exit 1
}

if npm install --production; then
    :
else
    echo "Failed to install npm"
    cleanup
    exit 1
fi

# Create the version file
echo "1.0.0" >version.txt

# Create the project's archive
if tar -cvzf "$HOME/project.tgz" .; then
    :
else
    echo "Failed to create the tar archive"
    cleanup
    exit 1
fi

# Final cleanup
cleanup
