## First Script

Create a script that - when run from the WSL virtual machine - will:

* Create a temporary sub-folder in the /tmp directory
* In that directory, git clone the nodejs you've created earlier
* Run the `npm install --production` in the clone folder
* Create a file at the root of the clone folder, called "version.txt". The content of this file will be the string "1.0.0".
* create a archive tar.gz or tgz containing the cloned folder
* Move that archive file in your home directory in WSL

## Second script

Create a script which will take an argument in the command line, that will be a filename.

This script will:

* read its first passed argument
* copy the filename given as the first argument and copy that file into your home folder. If the archive already exists in the destination of the copy, if will overwrite it
* extract the content of the archive file
* run the start command of your nodejs project


## That was too easy

If you already have experience with bash scripting you can:

* Ensure that your script has no linting warning in VScode from shellcheck
* Create dedicated logs file for your script. This log files will be managed by the script itself. You may create two scripts: one for the normal output, one for the error output.
* Ensure that when any command fails, the scripts exit with the 1 exit code.
